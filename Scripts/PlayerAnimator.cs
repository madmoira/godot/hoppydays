using Godot;
using System;

public class PlayerAnimator : AnimatedSprite {
    public void update(Vector2 motion, bool isOnFloor) {
        if (!isOnFloor) {
            Play("Jump");
            FlipH = false;
        } else if (motion.x > 0) {
            Play("Run");
            FlipH = false;
        } else if (motion.x < 0) {
            Play("Run");
            FlipH = true;
        } else {
            Play("Idle");
            FlipH = false;
        }
    }
}
