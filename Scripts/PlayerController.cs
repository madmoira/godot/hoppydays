using Godot;

public class PlayerController : KinematicBody2D {
    private Vector2 motion;
    private const int speed = 750;
    private const int gravity = 3600;
    private const int jumpSpeed = -1700;
    private Vector2 up = new Vector2(0, -2);

    [Export]
    int worldEnd = 3200;

    PlayerAnimator animationNode;
    GameState gameState;

    public override void _Ready() {
        var global = (Global)GetNode("/root/Global");
        global.pController = this;
        animationNode = GetNode<PlayerAnimator>("AnimatedSprite");
        gameState = GetOwner<GameState>();
    }

    public override void _Process(float delta) {
        animationNode.update(motion, IsOnFloor());
    }

    public override void _PhysicsProcess(float delta) {
        fall(delta);
        run();
        jump();
        MoveAndSlide(motion, up);
    }

    private void run() {
        if (Input.IsActionPressed("ui_right") && !Input.IsActionPressed("ui_left")) {
            motion.x = speed;
        } else if (Input.IsActionPressed("ui_left") && !Input.IsActionPressed("ui_right")) {
            motion.x = -speed;
        } else {
            motion.x = 0;
        }
    }

    private void fall(float delta) {
        if (IsOnFloor() || IsOnCeiling()) {
            motion.y = 100;
        } else {
            motion.y += gravity * delta;
        }

        if (Position.y > worldEnd) {
            gameState.endGame();;
        }
    }

    private void jump() {
        if (Input.IsActionJustPressed("ui_up") && IsOnFloor()) {
            motion.y += jumpSpeed;
        }
    }
}
