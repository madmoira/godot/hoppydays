using Godot;
using System;

public class GameState : Node2D {

    public override void _Ready() {
        var global = (Global)GetNode("/root/Global");
        global.gameState = this;
    }

    public void endGame() {
        GetTree().ChangeScene("res://Scenes/Levels/GameOver.tscn");
    }
}
